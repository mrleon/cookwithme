﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public static class NumberFunc
    {
        public static int AverageToInt(double avg)
        {
            if (avg - Math.Floor(avg) >= 0.5)
            {
                return Convert.ToInt32(Math.Ceiling(avg));
            }
            else
            {
                return Convert.ToInt32(Math.Floor(avg));
            }
        }
    }
}
