﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Library
{
    public static class StrFunc
    {
        /// <summary>
        /// Returns maxLength chars of string or the same, if count lower than maxLength
        /// </summary>
        /// <param name="str"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static string TruncateLongString(this string str, int maxLength)
        {
            return str.Substring(0, Math.Min(str.Length, maxLength));
        }

        public static string FormaSlowaKomentarze(int ile)
        {
            switch (ile)
            {
                case 1:
                    return "komentarz";
                case 2:
                case 3:
                case 4:
                    return "komentarze";
                default:
                    return "komentarzy";
            }
        }

        public static string RemoveHtmlTags(string text)
        {
            return Regex.Replace(text, "<.*?>", string.Empty);
        }

        
    }
}
