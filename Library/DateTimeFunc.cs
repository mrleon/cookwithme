﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public static class DateTimeFunc
    {
        public static string TimeElapsed(DateTime dt)
        {
            TimeSpan ts = DateTime.Now.Subtract(dt);
            if (ts.TotalDays >= 1)
            {
                if (ts.TotalDays == 1)
                {
                    return "1 dzień temu";
                }
                else
                {
                    return ((int)ts.TotalDays) + " dni temu";
                }
            }
            else if (ts.TotalHours >= 1)
            {
                if (ts.TotalHours <= 2)
                {
                    return "1 godzinę temu";
                }
                else if (ts.TotalHours >= 5)
                {
                    return ((int)ts.TotalHours) + " godzin temu";
                }
                else
                {
                    return ((int)ts.TotalHours) + " godziny temu";
                }
            }
            else
            {
                if (ts.TotalMinutes <= 2)
                {
                    return "1 minutę temu";
                }
                else if (ts.TotalMinutes >= 5)
                {
                    return ((int)ts.TotalMinutes) + " minut temu";
                }
                else
                {
                    return ((int)ts.TotalMinutes) + " minuty temu";
                }
            }
        }
    }
}
