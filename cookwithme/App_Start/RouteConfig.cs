﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


namespace cookwithme
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(name: "Wyszukiwarka",
                url: "Wyszukaj",
                defaults: new { controller = "Dishes", action = "Search" }
                );
            routes.MapRoute(name: "Statystyki",
                url: "Statystyki",
                defaults: new { controller = "Statistics", action = "Index" }
                );
            routes.MapRoute(name: "Pelny obrazek",
                url: "Image/{ID}",
                defaults: new { controller = "Dishes", action = "GetImage", ID = UrlParameter.Optional }
                );
            routes.MapRoute(name: "Miniaturka obrazek",
                url: "Thumbnail/{ID}",
                defaults: new { controller = "Dishes", action = "GetThumb", ID = UrlParameter.Optional }
                );
            routes.MapRoute(name: "Dodaj komentarz",
                url: "Komentarze/Dodaj",
                defaults: new { controller = "Dishes", action = "AddComment" }
                );
            routes.MapRoute(name: "Dodaj ocene",
                url: "Potrawy/{id}/Ocen",
                defaults: new { controller = "Dishes", action = "VoteForDish", id = UrlParameter.Optional }
                );
            routes.MapRoute(name: "Zarzadzaj uzytkownikami",
                url: "Uzytkownicy/Zarzadzaj",
                defaults: new { controller = "Account", action = "Manage" }
                );
            routes.MapRoute(name: "Ustaw moderatora",
                url: "Uzytkownicy/Zarzadzaj/UstawModeratora/{name}",
                defaults: new { controller = "Account", action = "SetModerator", name = UrlParameter.Optional }
                );
            routes.MapRoute(name: "Usun moderatora",
                url: "Uzytkownicy/Zarzadzaj/UsunModeratora/{name}",
                defaults: new { controller = "Account", action = "RemoveModerator", name = UrlParameter.Optional }
                );
            routes.MapRoute(name: "Potrawy Strony",
                url: "Potrawy/Strony/{pageNumber}",
                defaults: new { controller = "Dishes", action = "GetDishesForPage", pageNumber = UrlParameter.Optional }
                );
            routes.MapRoute(name: "Zarzadzaj potrawami",
                url: "Potrawy/Zarzadzaj",
                defaults: new { controller = "Dishes", action = "Manage" }
                );
            routes.MapRoute(name: "Potrawa/Nowa Potrawa",
                url: "Potrawy/Dodaj",
                defaults: new { controller = "Dishes", action = "Create" }
                );
            routes.MapRoute(name: "ID without action",
                url: "Potrawy/{id}",
                defaults: new { controller = "Dishes", action = "Details", id = UrlParameter.Optional }
                );
            routes.MapRoute(name: "Potrawa/Edytuj potrawe",
                url: "Potrawy/{id}/Edytuj",
                defaults: new { controller = "Dishes", action = "Edit", id = UrlParameter.Optional }
                );
            routes.MapRoute(name: "Potrawa/Usun potrawe",
                url: "Potrawy/{id}/Usun",
                defaults: new { controller = "Dishes", action = "Delete", id = UrlParameter.Optional }
                );
            routes.MapRoute(name: "Rejestracja",
                url: "Rejestracja",
                defaults: new { controller = "Account", action = "Register" }
                );
            routes.MapRoute(name: "Logowanie",
                url: "Logowanie",
                defaults: new { controller = "Account", action = "Login" }
                );
            routes.MapRoute(name: "Only category",
                url: "Kategorie",
                defaults: new { controller = "Categories", action = "Index" }
                );
            routes.MapRoute(name: "Categories/ID",
                url: "Kategorie/{id}",
                defaults: new { controller = "Categories", action = "Details", id = UrlParameter.Optional }
                );
            routes.MapRoute(name: "Categories/ID/Page",
                url: "Kategorie/{categoryId}/{pageNumber}",
                defaults: new { controller = "Dishes", action = "GetCategoryDishesForPage", categoryId = UrlParameter.Optional, pageNumber = UrlParameter.Optional }
                );
            routes.MapRoute(name: "Zapomniane hasło",
                url: "Resetowanie_hasla",
                defaults: new { controller = "Account", action = "ForgottenPassword" }
                );
            routes.MapRoute(name: "Nowe hasło",
                url: "Nowe_haslo/{code}",
                defaults: new { controller = "Account", action = "ResetPassword", code = UrlParameter.Optional }
                );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Dishes", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
