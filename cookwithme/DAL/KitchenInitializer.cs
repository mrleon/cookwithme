﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using cookwithme.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Drawing;
using Library;

namespace cookwithme.DAL
{
    public class KitchenInitializer : DropCreateDatabaseAlways<KitchenContext>
    {
        protected override void Seed(KitchenContext context)
        {
            #region Użytkownicy

            var name = "Admin";
            var email = "admin@admin.pl";
            var password = new PasswordHasher().HashPassword("password");
            var name2 = "User";
            var email2 = "bartek@google.pl";
            var password2 = new PasswordHasher().HashPassword("password");
            var name3 = "User2";
            var email3 = "john@gmail.com";
            var password3 = new PasswordHasher().HashPassword("password");
            var users = new List<ApplicationUser>
            {
                new ApplicationUser{ UserName = name, Email = email, PasswordHash = password, registered = DateTime.Now, banned = false, SecurityStamp = Guid.NewGuid().ToString() },
                new ApplicationUser{ UserName = name2, Email = email2, PasswordHash = password2, registered = DateTime.Now, banned = false, SecurityStamp = Guid.NewGuid().ToString() },
                new ApplicationUser{ UserName = name3, Email = email3, PasswordHash = password3, registered = DateTime.Now, banned = false, SecurityStamp = Guid.NewGuid().ToString()},
            };
            users.ForEach(t => context.Users.Add(t));
            context.SaveChanges();

            #endregion

            string idOfAdmin = context.Users.First(t => t.UserName == "Admin").Id;
            string idOfUser = context.Users.First(t => t.UserName == "User").Id;
            string idOfUser2 = context.Users.First(t => t.UserName == "User2").Id;

            #region Role
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            RoleManager.Create(new IdentityRole("Administrator"));
            RoleManager.Create(new IdentityRole("Moderator"));
            RoleManager.Create(new IdentityRole("User"));

            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            UserManager.AddToRoles(idOfAdmin, "Administrator", "Moderator", "User");
            UserManager.AddToRoles(idOfUser, "User");
            UserManager.AddToRoles(idOfUser2, "User");
            #endregion

            #region Kategorie
            var categories = new List<Category>
            {
                new Category{name="Przetwory"},
                new Category{name="Alkohole"},
                new Category{name="Ciasta i słodkości"},
                new Category{name="Sałatki"},
                new Category{name="Dania główne"},
                new Category{name="Zupy"},
                new Category{name="Desery i lody"},
                new Category{name="Pasty"},
                new Category{name="Przystawki"},
                new Category{name="Surówki"},
                new Category{name="Napoje"},
                new Category{name="Inne"},
            };
            categories.ForEach(t => context.Categories.Add(t));
            context.SaveChanges();
            #endregion

            Dictionary<string, int> kategorie = new Dictionary<string, int>();
            context.Categories.ToList().ForEach(t => kategorie.Add(t.name, t.ID));

            #region Dania

            var dishes = new List<Dish>
            {
                new Dish{ApplicationUserID = idOfUser, CategoryID=kategorie["Dania główne"], title="Bigos", ingredients=bigosSkladniki, description=bigosOpis, level=Difficulty.Easy, created=new DateTime(2014,10,21,12,0,0), accepted = true},
                
                new Dish{ApplicationUserID = idOfUser, CategoryID=kategorie["Zupy"], title="Pomidorowa", ingredients=pomidorowaSkladniki, description=pomidorowaOpis, level=Difficulty.Hard, created=new DateTime(2014,10,21,13,0,0), accepted = true},
                new Dish{ApplicationUserID = idOfUser, CategoryID=kategorie["Zupy"], title="Rosół", ingredients=rosolSkladniki, description=rosolOpis, level=Difficulty.Medium, created=new DateTime(2014,10,21,14,0,0), accepted = true},
                
                new Dish{ApplicationUserID = idOfUser2, CategoryID=kategorie["Dania główne"], title="Kotlet schabowy", ingredients=schabowySkladniki, description=schabowyOpis, level=Difficulty.Very_easy, created=new DateTime(2014,10,21,15,0,0), accepted = true},
                
                new Dish{ApplicationUserID = idOfUser2, CategoryID=kategorie["Desery i lody"], title="Lody czekoladowe", ingredients=lodyCzekoladoweSkladniki, description=lodyCzekoladoweOpis, level=Difficulty.Very_hard, created=new DateTime(2014,10,21,16,0,0), accepted = true},
            
                new Dish{ApplicationUserID = idOfUser, CategoryID=kategorie["Przetwory"], title="Domowy dżem z truskawek", ingredients=dzemSkladniki, description=dzemOpis, level=Difficulty.Medium, created=new DateTime(2014,10,21,16,0,0), accepted = true},
                new Dish{ApplicationUserID = idOfUser2, CategoryID=kategorie["Przetwory"], title="Powidła śliwkowe", ingredients=powidlaSkladniki, description=powidlaOpis, level=Difficulty.Very_hard, created=new DateTime(2014,10,21,16,0,0), accepted = true},
                
                new Dish{ApplicationUserID = idOfUser, CategoryID=kategorie["Alkohole"], title="Nalewka ze śliwek", ingredients=nalewkaSkladniki, description=nalewkaOpis, level=Difficulty.Very_hard, created=new DateTime(2014,10,21,16,0,0), accepted = true},
                new Dish{ApplicationUserID = idOfUser2, CategoryID=kategorie["Alkohole"], title="Malibu", ingredients=malibuSkladniki, description=malibuOpis, level=Difficulty.Very_hard, created=new DateTime(2014,10,21,16,0,0), accepted = true},
                
                new Dish{ApplicationUserID = idOfUser, CategoryID=kategorie["Ciasta i słodkości"], title="Szarlotka", ingredients=szarlotkaSkladniki, description=SzarlotkaOpis, level=Difficulty.Very_hard, created=new DateTime(2014,10,21,16,0,0), accepted = true},
                new Dish{ApplicationUserID = idOfUser2, CategoryID=kategorie["Ciasta i słodkości"], title="Rogaliki drożdżowe", ingredients=rogalikiSkladniki, description=rogalikiOpis, level=Difficulty.Very_hard, created=new DateTime(2014,10,21,16,0,0), accepted = true},
                
                new Dish{ApplicationUserID = idOfUser, CategoryID=kategorie["Desery i lody"], title="Lody waniliowe", ingredients=lodyWanilioweSkladniki, description=lodyWanilioweOpis, level=Difficulty.Very_hard, created=new DateTime(2014,10,21,16,0,0), accepted = true},

                new Dish{ApplicationUserID = idOfUser2, CategoryID=kategorie["Sałatki"], title="Sałatka warzywna", ingredients=salatkaWarzywnaSkladniki, description=salatkaWarzywnaOpis, level=Difficulty.Very_hard, created=new DateTime(2014,10,21,16,0,0), accepted = true},
                new Dish{ApplicationUserID = idOfUser, CategoryID=kategorie["Sałatki"], title="Sałatka owocowa", ingredients=salatkaOwocowaSkladniki, description=salatkaOwocowaOpis, level=Difficulty.Very_hard, created=new DateTime(2014,10,21,16,0,0), accepted = true},

                new Dish{ApplicationUserID = idOfUser2, CategoryID=kategorie["Pasty"], title="Pasta jajeczna", ingredients=pastaJajecznaSkladniki, description=pastaJajecznaOpis, level=Difficulty.Very_hard, created=new DateTime(2014,10,21,16,0,0), accepted = true},
                new Dish{ApplicationUserID = idOfUser, CategoryID=kategorie["Pasty"], title="Pasta łososiowa", ingredients=pastaLososiowaSkladniki, description=pastaLososiowaOpis, level=Difficulty.Very_hard, created=new DateTime(2014,10,21,16,0,0), accepted = true},

                new Dish{ApplicationUserID = idOfUser2, CategoryID=kategorie["Przystawki"], title="Kulki ziemniaczane w karmelu", ingredients=kulkiZiemniaczaneSkladniki, description=kulkiZiemniaczaneOpis, level=Difficulty.Very_hard, created=new DateTime(2014,10,21,16,0,0), accepted = true},
                new Dish{ApplicationUserID = idOfUser, CategoryID=kategorie["Przystawki"], title="Papryka faszerowana", ingredients=paprykaFaszerowanaSkladniki, description=paprykaFaszerowanaOpis, level=Difficulty.Very_hard, created=new DateTime(2014,10,21,16,0,0), accepted = true},

            };
            dishes.ForEach(t => context.Dishes.Add(t));
            context.SaveChanges();
            #endregion

            #region Obrazki

            var pictures = new List<DishPicture>
            {
                new DishPicture //BIGOS
                {
                    ID = 1,
                    BigImage = ImgFunc.ImageToByteArray(Image.FromFile(HttpContext.Current.Server.MapPath("~/DAL/ImageSeed/bigos.jpg"))),
                    BigMime = "image/jpg",
                    ThumbImage = new byte[0],
                    ThumbMime = "",
                },
                new DishPicture // POMIDOROWA
                {
                    ID = 2, 
                    BigImage = ImgFunc.ImageToByteArray(Image.FromFile(HttpContext.Current.Server.MapPath("~/DAL/ImageSeed/pomidorowa.jpg"))),
                    BigMime = "image/jpg",
                    ThumbImage = new byte[0],
                    ThumbMime = "",
                },
                new DishPicture // ROSOL
                {
                    ID = 3, 
                    BigImage = ImgFunc.ImageToByteArray(Image.FromFile(HttpContext.Current.Server.MapPath("~/DAL/ImageSeed/rosol.jpg"))),
                    BigMime = "image/jpg",
                    ThumbImage = new byte[0],
                    ThumbMime = "",
                },
                new DishPicture //SCHABOWY
                {
                    ID = 4, 
                    BigImage = ImgFunc.ImageToByteArray(Image.FromFile(HttpContext.Current.Server.MapPath("~/DAL/ImageSeed/schabowy.jpg"))),
                    BigMime = "image/jpg",
                    ThumbImage = new byte[0],
                    ThumbMime = "",
                },
                new DishPicture // LODY CZEKOLADOWE
                {
                    ID = 5, 
                    BigImage = ImgFunc.ImageToByteArray(Image.FromFile(HttpContext.Current.Server.MapPath("~/DAL/ImageSeed/lodyczekoladowe.jpg"))),
                    BigMime = "image/jpg",
                    ThumbImage = new byte[0],
                    ThumbMime = "",
                },
                new DishPicture // DŻEM
                {
                    ID = 6, 
                    BigImage = ImgFunc.ImageToByteArray(Image.FromFile(HttpContext.Current.Server.MapPath("~/DAL/ImageSeed/dzem.jpg"))),
                    BigMime = "image/jpg",
                    ThumbImage = new byte[0],
                    ThumbMime = "",
                },
                new DishPicture // POWIDŁA
                {
                    ID = 7, 
                    BigImage = ImgFunc.ImageToByteArray(Image.FromFile(HttpContext.Current.Server.MapPath("~/DAL/ImageSeed/powidla.jpg"))),
                    BigMime = "image/jpg",
                    ThumbImage = new byte[0],
                    ThumbMime = "",
                },
                new DishPicture // NALEWKA
                {
                    ID = 8, 
                    BigImage = ImgFunc.ImageToByteArray(Image.FromFile(HttpContext.Current.Server.MapPath("~/DAL/ImageSeed/nalewka.jpg"))),
                    BigMime = "image/jpg",
                    ThumbImage = new byte[0],
                    ThumbMime = "",
                },
                new DishPicture // MALIBU 
                {
                    ID = 9, 
                    BigImage = ImgFunc.ImageToByteArray(Image.FromFile(HttpContext.Current.Server.MapPath("~/DAL/ImageSeed/malibu.jpg"))),
                    BigMime = "image/jpg",
                    ThumbImage = new byte[0],
                    ThumbMime = "",
                },
                new DishPicture // SZARLOTKA
                {
                    ID = 10, 
                    BigImage = ImgFunc.ImageToByteArray(Image.FromFile(HttpContext.Current.Server.MapPath("~/DAL/ImageSeed/szarlotka.jpg"))),
                    BigMime = "image/jpg",
                    ThumbImage = new byte[0],
                    ThumbMime = "",
                },
                new DishPicture // ROGALIKI
                {
                    ID = 11, 
                    BigImage = ImgFunc.ImageToByteArray(Image.FromFile(HttpContext.Current.Server.MapPath("~/DAL/ImageSeed/rogaliki.jpg"))),
                    BigMime = "image/jpg",
                    ThumbImage = new byte[0],
                    ThumbMime = "",
                },
                new DishPicture // LODY WANILIOWE
                {
                    ID = 12, 
                    BigImage = ImgFunc.ImageToByteArray(Image.FromFile(HttpContext.Current.Server.MapPath("~/DAL/ImageSeed/lodywaniliowe.jpg"))),
                    BigMime = "image/jpg",
                    ThumbImage = new byte[0],
                    ThumbMime = "",
                },
                new DishPicture // SAŁATKA WARZYWNA
                {
                    ID = 13, 
                    BigImage = ImgFunc.ImageToByteArray(Image.FromFile(HttpContext.Current.Server.MapPath("~/DAL/ImageSeed/salatkawarzywna.jpg"))),
                    BigMime = "image/jpg",
                    ThumbImage = new byte[0],
                    ThumbMime = "",
                },
                new DishPicture // SAŁATKA OWOCOWA
                {
                    ID = 14, 
                    BigImage = ImgFunc.ImageToByteArray(Image.FromFile(HttpContext.Current.Server.MapPath("~/DAL/ImageSeed/salatkaowocowa.jpg"))),
                    BigMime = "image/jpg",
                    ThumbImage = new byte[0],
                    ThumbMime = "",
                },
                new DishPicture // PASTA JAJECZNA
                {
                    ID = 15, 
                    BigImage = ImgFunc.ImageToByteArray(Image.FromFile(HttpContext.Current.Server.MapPath("~/DAL/ImageSeed/pastajajeczna.jpg"))),
                    BigMime = "image/jpg",
                    ThumbImage = new byte[0],
                    ThumbMime = "",
                },
                new DishPicture // PASTA ŁOSOSIOWA
                {
                    ID = 16, 
                    BigImage = ImgFunc.ImageToByteArray(Image.FromFile(HttpContext.Current.Server.MapPath("~/DAL/ImageSeed/pastalososiowa.jpg"))),
                    BigMime = "image/jpg",
                    ThumbImage = new byte[0],
                    ThumbMime = "",
                },
                new DishPicture // KULKI ZIEMNIACZANE
                {
                    ID = 17, 
                    BigImage = ImgFunc.ImageToByteArray(Image.FromFile(HttpContext.Current.Server.MapPath("~/DAL/ImageSeed/kulki.jpg"))),
                    BigMime = "image/jpg",
                    ThumbImage = new byte[0],
                    ThumbMime = "",
                },
                new DishPicture // PAPRYKA FASZEROWANA
                {
                    ID = 18, 
                    BigImage = ImgFunc.ImageToByteArray(Image.FromFile(HttpContext.Current.Server.MapPath("~/DAL/ImageSeed/papryka.jpg"))),
                    BigMime = "image/jpg",
                    ThumbImage = new byte[0],
                    ThumbMime = "",
                },
            };

            pictures.ForEach(t => context.Pictures.Add(t));
            context.SaveChanges();


            #endregion

            #region Komentarze
            var comments = new List<Comment>
            {
                new Comment{ApplicationUserID = idOfAdmin, DishID=1, content="brawo!1", added=new DateTime(2014,10,10,10,10,10)},
                new Comment{ApplicationUserID = idOfUser, DishID=1, content="brawo!2", added=new DateTime(2014,10,10,10,10,10)},
                new Comment{ApplicationUserID = idOfAdmin, DishID=1, content="brawo!3", added=new DateTime(2014,10,10,10,10,10)},
                new Comment{ApplicationUserID = idOfUser, DishID=1, content="brawo!4", added=new DateTime(2014,10,10,10,10,10)},
                new Comment{ApplicationUserID = idOfAdmin, DishID=2, content="brawo!5", added=new DateTime(2014,10,10,10,10,10)},
                new Comment{ApplicationUserID = idOfUser, DishID=2, content="brawo!6", added=new DateTime(2014,10,10,10,10,10)},
                new Comment{ApplicationUserID = idOfAdmin, DishID=2, content="brawo!7", added=new DateTime(2014,10,10,10,10,10)},
                new Comment{ApplicationUserID = idOfAdmin, DishID=3, content="brawo!8", added=new DateTime(2014,10,10,10,10,10)},
                new Comment{ApplicationUserID = idOfAdmin, DishID=4, content="brawo!9", added=new DateTime(2014,10,10,10,10,10)},
                new Comment{ApplicationUserID = idOfAdmin, DishID=5, content="brawo!10", added=new DateTime(2014,10,10,10,10,10)},
                new Comment{ApplicationUserID = idOfUser, DishID=5, content="brawo!11", added=new DateTime(2014,10,10,10,10,10)},
                new Comment{ApplicationUserID = idOfAdmin, DishID=5, content="brawo!12", added=new DateTime(2014,10,10,10,10,10)},
                new Comment{ApplicationUserID = idOfUser, DishID=5, content="brawo!12", added=new DateTime(2014,10,10,10,10,10)},
                new Comment{ApplicationUserID = idOfUser, DishID=5, content="brawo!13", added=new DateTime(2014,10,10,10,10,10)},
            };
            comments.ForEach(t => context.Comments.Add(t));
            context.SaveChanges();
            #endregion

            #region Głosy
            var votes = new List<Vote>
            {
                new Vote{DishID=1, ocena=1, ApplicationUserID=idOfAdmin},
                new Vote{DishID=1, ocena=5, ApplicationUserID=idOfUser},
                new Vote{DishID=3, ocena=3, ApplicationUserID=idOfAdmin},
                new Vote{DishID=4, ocena=4, ApplicationUserID=idOfAdmin},
                new Vote{DishID=5, ocena=2, ApplicationUserID=idOfAdmin},
            };
            votes.ForEach(t => context.Votes.Add(t));
            context.SaveChanges();
            #endregion

            #region Ustawienia

            var settings = new List<Setting>
            {
                new Setting{ Type = "NumberOfDishesForPage", Value = "15" },
                new Setting{ Type = "NumberOfDishesForCategoryPage", Value = "15" },
            };

            settings.ForEach(t => context.Settings.Add(t));
            context.SaveChanges();

            #endregion

        }

        #region Składniki i opisy

        private const string bigosSkladniki = "<p>&nbsp;</p><h3><strong>Składniki</strong></h3><ul><li>1.5 kg kiszonej kapusty</li><li>0.5 kg wieprzowiny (od szynki)</li><li>0.5 kg wołowiny</li><li>30 dkg gotowanej szynki</li><li>0.5 kg r&oacute;żnych kiełbas (np. zwyczajna , wiejska, toruńska , litewska)</li><li>12 suszonych prawdziwk&oacute;w</li><li>10 suszonych śliwek</li><li>5 ziaren pieprzu i 5 ziaren ziela angielskiego</li><li>2 liście laurowe</li><li>5 rozgniecionych owoc&oacute;w jałowca</li><li>2 cebule</li><li>łyżka miodu ( lub cukru)</li><li>kieliszek czerwonego wytrawnego wina</li><li>s&oacute;l , pieprz</li><li>smalec</li></ul>";
        private const string bigosOpis = "<h3><span class='title-bar bar-brown'>Etapy przygotowania</span></h3><p>&nbsp;</p><ul><li><p>Kiszoną kapustę posiekać niezbyt drobno, zalać niewielką ilością wody i gotować na małym ogniu z zielem ang., liścami laurowymi, pieprzem i owocami jałowca ok 1.5 h.</p></li></ul><p>&nbsp;</p><ul><li><p>Grzyby ugotować w małej ilości wody ok 20 minut. Pokroić w paski a wywar wlać do kapusty.</p><div class='cl'>&nbsp;</div></li><li><p>Wieprzowinę i wołowinę pokroić w kawałki i podsmażyć na smalcu na złoty kolor. Cebulę drobno posiekać , przysmażyć na smalcu. Kiełbasę pokrojoną w talarki i małe plasterki szynki podsmażyć lekko na patelni. Wrzucić podsmażone mięso, kiełbasę z szynką , grzyby , cebulę do kapusty i dalej gotować na małym ogniu godzinę ( mięso powinno być miękkie)</p><div class='cl'>&nbsp;</div></li><li><p>Pod koniec gotowania dodać do bigosu pokrojone suszone śliwki , mi&oacute;d i wino. Doprawić solą i pieprzem.</p><div class='cl'>&nbsp;</div></li><li><p>Bigos ma już taki urok, że tym smaczniejszy , im więcej razy podgrzewany!!</p></li></ul><p>&nbsp;</p><p>&nbsp;</p>";

        private const string schabowySkladniki = "<h3><strong>Składniki</strong></h3><ul><li>jajka</li><li>smalec</li><li>przyprawy (s&oacute;l i pieprz)</li><li>schab z kością lub bez kości</li><li>bułka tarta</li><li>mąka</li></ul>";
        private const string schabowyOpis = "<p>Schab kroimy na plastry i ubijamy tłuczkiem. Jajka roztrzepujemy i łączymy z przyprawami. Ubity i pokrojony schab otaczamy w mące, potem w jajku, a następnie w bułce tartej. Wstawiamy patelnię na średni ogień i dajemy smalec. Przygotowany kotlet kładziemy na patelnię i smażymy na złoty kolor z jednej i drugiej strony. Podajemy na talerzu z ziemniakami, kaszą, ryżem lub kluskami oraz duszonymi lub smażonymi pieczarkami, gotowanymi jarzynami, sur&oacute;wkami, marynatami itp.</p>";

        private const string pomidorowaSkladniki = "<div class='ilosc_porcji'><h3>Składniki</h3></div><ul><li>ż&oacute;łta papryka</li><li>filet z piersi kurczaka</li><li>cebula</li><li>3 ząbki czosnku</li><li>garść fasolki szparagowej</li><li>olej</li><li>szklanka bulionu drobiowego</li><li>1/2 l passaty pomidorowej</li><li>puszka krojonych pomidor&oacute;w</li><li>2 łyżki brązowego cukru</li><li>puszka ciecierzycy</li><li>mielone chili</li><li>przyprawa 5 smak&oacute;w</li><li>s&oacute;l</li><li>koncentrat pomidorowy (niekoniecznie)</li><li>kilka gałązek natki</li></ul>";
        private const string pomidorowaOpis = "<h3>Etapy przygotowania</h3><p><strong>Pomidorowa</strong></p><p>&nbsp;</p><p>Paprykę kroimy w kostkę, mięso w paski, cebulę i czosnek siekamy, oczyszczoną fasolkę tniemy na 2&ndash;3 części. Paprykę, cebulę i czosnek podsmażamy przez chwilę na łyżce oleju, dokładamy fasolkę, zalewamy bulionem.</p><p>Kiedy się zagotuje, dokładamy passatę, pomidory, cukier, osączoną ciecierzycę i przyprawy. Gotujemy pod przykryciem, aż fasolka będzie miękka. Mięso smażymy na złoto na patelni, wkładamy do zupy, gotujemy jeszcze chwilę. Jeśli zupa ma za słaby kolor, dodajemy trochę koncentratu, mieszamy z posiekaną natką.</p>";

        private const string rosolSkladniki = "<div class='ilosc_porcji'><h3>Składniki</h3></div><ul><li>1 kg wołowiny z kością (najlepiej szponder, pręga, łopatka lub mostek)</li><li>40 dag drobiowych części rosołowych (pierś, udka lub skrzydełka)</li><li>duża porcja włoszczyzny z kapustą (3 średniej wielkości marchewki, p&oacute;ł selera korzeniowego, duży korzeń pietruszki, średni por, kawałek kapusty włoskiej)</li><li>cebula</li><li>duży liść laurowy</li><li>5&ndash;6 ziarenek pieprzu</li><li>pęczek natki pietruszki</li><li>s&oacute;l</li></ul>";
        private const string rosolOpis = "<h3>Etapy przygotowania</h3><p>Przepis na klasyczny <strong>ros&oacute;ł</strong></p><p>Do dużego garnka włożyć na początek wołowinę. Zalać ok. 3,5 l zimnej wody. Lekko osolić. Postawić na małym ogniu i tak ustawić temperaturę, by mięso wolno się gotowało. Łyżką cedzakową systematycznie usuwać zbierającą się szumowinę. Po około 30 minutach dodać części rosołowe kury i dalej gotować, odszumowując.</p><p>Marchewkę, seler i pietruszkę obrać, starannie wypłukać i przełożyć do salaterki. Z kapusty i pora usunąć zewnętrzne liście, a resztę dokładnie wypłukać pod bieżącą wodą. Cebulę obrać, opalić na ogniu lub suchej patelni, tak aby wierzchnia warstwa cebuli sczerniała. Gdy wywar mięsny stanie się już klarowny, wrzucić liść laurowy, ziarnka pieprzu, opaloną cebulę oraz obrane warzywa. Przykryć i gotować wolniutko 1,5 &ndash; 2 godziny, aby ros&oacute;ł tylko &bdquo;pyrkał&rdquo;, i im wolniej, tym lepiej.</p><p>Gdy minie wyznaczony czas, spr&oacute;bować ros&oacute;ł i ocenić, jak mocno go przyprawić. Na og&oacute;ł wystarczy trochę soli i odrobina pieprzu. (Niekt&oacute;rzy, przyzwyczajeni do smaku gotowych przypraw, zastępują s&oacute;l kostką rosołową lub przyprawą typu vegeta). Następnie przykryć garnek i niech wszystko gotuje się, aż mięso będzie zupełnie miękkie, tzn. bez trudu da się oddzielić od kości. Przecedzić ros&oacute;ł do dobrze nagrzanej wazy <br />(nie powinien wystygnąć przed podaniem!). Podawać z makaronem, posiekaną natką pietruszki i plasterkami marchewki.</p><p><strong>PAMIĘTAJ</strong></p><p>Na <strong>ros&oacute;ł </strong>wybieraj zawsze świeże mięso. Z mrożonego zazwyczaj powstaje nieco mętny. Z wołowiny czy cielęciny najlepiej nadają się dość chude kawałki mięsa z przednich części tuszy.</p><p><strong>Ros&oacute;ł </strong>możesz ugotować z innych gatunk&oacute;w mięs, ewentualnie tylko z samej kury. Wszystko zależy od tego, jak tłusty i esencjonalny chcesz uzyskać. W staropolskiej kuchni <strong>prawdziwy ros&oacute;ł </strong>gotowano na kościach wołowych i cielęcych oraz kilku gatunkach mięsa.</p><p>Z esencjonalnego wywaru z kości, mięsa, warzyw i przypraw, po ostudzeniu, dokładnym odtłuszczeniu i przecedzeniu przez gęste sito, otrzymasz bulion.</p><p>Smak bulionu możesz wzbogacić r&oacute;żnymi dodatkami, na przykład suszonymi grzybami lub śliwkami, czosnkiem, karmelem (dla bardziej intensywnej barwy), zielem angielskim, jałowcem, ewentualnie małą gałązką lubczyku.</p><p>Szumowinę (wartościowe ścięte białko) zebraną łyżką cedzakową możesz dodać do gulaszu lub bigosu, jeśli akurat je gotujesz.</p><p><strong>Ros&oacute;ł </strong>nie jest potrawą błyskawiczną, nie znosi przyspieszania i żadnych rozwiązań na skr&oacute;ty. Musi gotować się długo i powoli. W przeciwnym razie stanie się mętny i nie uzyska pożądanego intensywnego smaku.</p><p>Mięsa i warzyw, kt&oacute;re zostały na sicie, nie wyrzucaj. Część warzyw możesz podać w rosole lub przeznaczyć na tradycyjną sałatkę z majonezem. Pozostałe warzywa możesz zemleć wraz z mięsem, przyprawić i użyć jako farszu, np. do pierożk&oacute;w, naleśnik&oacute;w lub warzyw.</p><p>Taki pyszny <strong>ros&oacute;ł </strong>zasmakuje każdemu.</p><p>&nbsp;Smacznego!</p>";

        private const string dzemSkladniki = "<ul><li>1 kg truskawek</li><li>1/2 kg cukru</li></ul>";
        private const string dzemOpis = "<p>Truskawki umyj, usuń szypułki. Następnie owoce rozgnieć widelcem lub zmiksuj, przeł&oacute;ż do garnka i zasyp cukrem. Przykryj i wł&oacute;ż na ok. 2 godz. do lod&oacute;wki. Po tym czasie gotuj ok. 1 godz. na wolnym ogniu, aż dżem zgęstnieje, możesz dodać kilka łyżek wody, aby łatwiej się gotowały. Ugotowane natychmiast przeł&oacute;ż do wyparzonych słoiczk&oacute;w. Po kilku minutach odwr&oacute;ć słoiczki do g&oacute;ry dnem do czasu aż przestygną. Dżem nadaje się do spożycia zaraz po wystygnięciu. Przechowuj w zacienionym, chłodnym miejscu.</p>";

        private const string powidlaSkladniki = "<p><strong>Składniki:</strong></p><ul><li>1 kg śliwek węgierek (najlepiej mocno dojrzałych)</li></ul><p><em>Dodatkowo:</em></p><ul><li>do 150g cukru (opcjonalnie)</li><li>&frac12; łyżeczki cynamonu (opcjonalni</li></ul>";
        private const string powidlaOpis = "<p><strong>Spos&oacute;b przygotowania:</strong></p><ol><li>Śliwki umyć, osuszyć, przeciąć na p&oacute;ł i wyciągnąć pestkę.</li><li>Śliwki włożyć do garnka dodać bardzo małą ilość wody. (Na 1 kg śliwek wystarczą 2 łyżki wody na dnie garnka).</li><li>Gotować na małej lub średniej mocy palnika, często mieszając, aby śliwki się nie przypaliły, a płyn szybciej odparował.</li><li>Powidła są gotowe, gdy masa jest na tyle gęsta, że spada z łyżki płatami, a ślad zrobiony łyżką po dnie garnka nie zlewa się. (Dla 1 kg śliwek proces ten trwa ok. 1,5 godz.).</li><li>Teraz należy sprawdzić słodkość powideł. Jeśli są odpowiednio słodkie należy gorące przełożyć do czystych słoik&oacute;w, mocno zakręcić i postawić słoiki do g&oacute;ry dnem na 15min. Jeśli są za mało słodkie należy dodać cukru do smaku. Można dodać r&oacute;wnież cynamon dla aromatu. (Dodałam 100g cukru, bo moje, kupne śliwki nie były bardzo słodkie).</li><li>Jeśli dodaliśmy cukier należy powidła dalej gotować, aż ponownie zgęstnieją, ponieważ cukier rozrzedza powidła, gdyż wyciąga ze śliwek resztę wody. (Gotowałam jeszcze 30min.).&nbsp;Następnie gorące przełożyć do wyparzonych, suchych słoik&oacute;w, mocno zakręcić i zapasteryzować.</li></ol>";

        private const string nalewkaSkladniki = "<p><strong>Składniki:</strong></p><ul><li>1kg śliwek</li><li>0,5 l w&oacute;dki</li><li>0,25 l spirytusu</li><li>laska wanilli</li><li>1,5 kubka cukur</li></ul>";
        private const string nalewkaOpis = "<p><strong>PRZYGOTOWANIE: </strong></p><p>Śliwki myjemy, usuwamy z nich pestki. Przekładamy je do naczynia (lub słoja), zalewamy alkoholem, dodajemy laskę wanilii&nbsp;i odstawiamy na ok. miesiąc. Co jakiś czas mieszamy. Po tym czasie alkohol zlewamy i odstawiamy. Śliwki zasypujemy cukrem na ok. 2 tygodnie &ndash; mieszamy. Cukier powinien się rozpuścić. Dokładnie odciskamy cały syrop. Łączymy go z wcześniej zlanym alkoholem i przelewamy do butelek. Możemy wszystko przefiltrować np. przez filtr do kawy. Nalewkę odstawiamy na 3 miesiące do p&oacute;ł roku. Im dłużej tym lepiej, wiadomo&hellip;&nbsp;</p>";

        private const string malibuSkladniki = "<p class='green bold'><strong>Składniki:</strong></p><ul class='ingredients block'><li>200g wi&oacute;rk&oacute;w kokosowych</li><li>250 ml spirytusu</li><li>1 puszka skondensowanego mleka słodzonego</li><li>1 puszka skondensowanego mleka niesłodzonego</li><li>opcjonalnie mleczko kokosowe</li><li>słoiczek z nakrętką</li></ul>";
        private const string malibuOpis = "<h4>Jak przygotować:</h4><p>Sprawdzony przepis na homemade malibu. Na imprezie zrobiło furorę. Wi&oacute;rki wsypujemy do słoiczka i zalewamy spirytusem, zakręcamy i zostawiamy tak na 4 dni. Po czterech dniach odsączamy wi&oacute;rki (można z nich zrobić placek - koleżanka podała taki na urodzinach i goście mieli dym jeszcze przed oficjalnym piciem XD ). Do dzbanka wlewamy obie puszki mleka oraz mleczko kokosowe (nie jest konieczne) i dolewamy odsączony przez sitko spirytus. Całość miksujemy i przelewamy do butelki. Gotowe do spożycia.</p><p>&nbsp;</p><p class='green bold'><strong>Informacje dodatkowe:</strong></p><p>Ważne! Zawsze wlewamy spirytus do mleka, a nie na odwr&oacute;t, bo może się zważyć.</p>";

        private const string szarlotkaSkladniki = "<p><strong>Ciasto:</strong></p><ul><li>350 g mąki pszennej tortowej (typ 450)</li><li>160 g miękkiego masła</li><li>4 łyżki mleka</li><li>2 jajka</li><li>1/2 szklanki cukru</li><li>2 pełne łyżeczki proszku do pieczenia</li><li>1 opakowanie cukru wanilinowego (32 g)</li></ul><p><strong>Masa jabłkowa:</strong></p><ul><li>2,5 kg jabłek (waga przed obraniem)</li><li>1/2 szklanki cukru (jeśli jabłka będą kwaśne, trzeba dodać więcej)</li><li>4 łyżki wody</li><li>2 cukry wanilinowe (po 32 g)</li><li>1 łyżeczka cynamonu</li><li>70 g masła</li><li>3 łyżki mąki ziemniaczanej</li></ul>";
        private const string SzarlotkaOpis = "<p>Jabłka obieramy ze sk&oacute;rki, wycinamy gniazda nasienne i kroimy je w średniej wielkości kostkę. Pokrojone jabłka przekładamy do garnka z grubym dnem, dodajemy wodę, masło, cukier zwykły oraz wanilinowy i masło. Dusimy do chwili aż jabłka zmiękną (ale nie powinny się rozpaść). Do miękkich jabłek dodajemy mąkę i ciągle mieszając prażymy jeszcze minutę.&nbsp;</p><p>Mąkę łączymy z proszkiem do pieczenia i cukrami. Dodajemy do niej jajka, mleko i masło i przy pomocy miksera łączymy składniki w jednolite ciasto. Ciasto będzie dość miękkie, ale nie należy dodawać więcej mąki, aby po upieczeniu nie było twarde.</p><p>Formę o wymiarach 25x30 cm wykładamy papierem do pieczenia. Ciasto dzielimy na dwie części i jedną z nich wkładamy do zamrażarki a drugą wykładamy sp&oacute;d formy. Ponieważ ciasto jest miękkie i ciężko byłoby je wałkować, najlepiej rozprowadzić je w foremce dłońmi. Nakłute widelcem ciasto wstawiamy do piekarnika nagrzanego do 180 stopni i pieczemy 10 minut.&nbsp;</p><p>Na podpieczony sp&oacute;d wykładamy jabłka i przykrywamy je schłodzonym, rozwałkowanym na podsypanej mąką stolnicy ciastem. Ciasto pieczemy kolejne 30-35 minut a następnie odstawiamy na co najmniej 12 godzin w chłodne miejsce, aby masa jabłkowa stężała.&nbsp;</p><p>Zimne ciasto polewamy lukrem przygotowanym ze szklanki cukru pudru oraz 4-5 łyżek wody.&nbsp;</p>";

        private const string rogalikiSkladniki = "<p><strong>Ciasto:</strong></p><ul><li>3,5 szklanki mąki pszennej tortowej (typ 450)</li><li>120 g masła</li><li>200 ml mleka</li><li>2 jajka</li><li>2/3 szklanki cukru pudru</li><li>2 opakowania cukru wanilinowego</li><li>1/3 łyżeczki soli</li><li>80 g świeżych drożdży</li></ul><p>&nbsp;</p><p><strong>Dodatkowo:&nbsp;</strong></p><ul><li>600 g marmolady o smaku r&oacute;żanym</li></ul>";
        private const string rogalikiOpis = "<p>Drożdże rozkruszamy do miseczki, dodajemy 1 łyżkę cukru pudru, 2 płaskie łyżki mąki i 5 łyżek letniego mleka (ok. 35-40 stopni). Zaczyn mieszamy i odstawiamy w ciepłe miejsce na ok. 20 minut.</p><p>Jajka ubijamy z pozostałym cukrem pudrem oraz cukrem wanilinowym na jasną, puszystą masę a następnie dodajemy do niej wyrośnięty zaczyn, s&oacute;l, ciepłe mleko, przesianą mąkę oraz stopione i przestudzone masło. Wyrabiamy gładkie, dośc luźne ciasto (przez około 8 minut), a następnie przekładamy je do wysmarowanej olejem miski, przykrywamy ściereczką i odstawiamy w ciepłe miejsce na godzinę.&nbsp;</p><p>Wyrośnięte ciasto dzielimy na 3 części i każdą z nich wałkujemy na podsypanej mąką stolnicy nadając ciastu kształt koła, kt&oacute;re dzielimy na 8 tr&oacute;jkąt&oacute;w. Na każdy z nich nakładamy płaską łyżeczkę marmolady, odrobinę ją rozsmarowujemy i zwijamy rogaliki. Blachę z piekarnika wykładamy papierem do pieczenia, opr&oacute;szamy mąką i układamy na niej rogaliki (po 9 sztuk na jedną partię). Wstawiamy je do nagrzanego do 180 stopni piekarnika i pieczemy 15 minut (nie dopuszczając, by zbytnio się przypiekły).</p><p>Rogaliki najlepiej smakują lekko ciepłe.&nbsp;</p>";

        private const string lodyCzekoladoweSkladniki = "<h3><strong>Składniki ( na ok. 1 l lod&oacute;w) :&nbsp; </strong></h3><ul><li>300 ml słodkiej śmietanki 30 - 36%</li><li>3 łyżki cukru</li><li>2 duże jajka (lub 3 małe)</li><li>łyżka wody</li><li>p&oacute;ł tabliczki gorzkiej czekolady&nbsp; o zawartości min. 60 % kakao, (cała, jeśli chcemy mieć mocno czekoladowy smak)&nbsp;</li><li>ew. wnętrze wyskrobane ze środka laski wanilii lub kilka kropli ekstraktu waniliowego (niekonieczne)</li><li>ew. p&oacute;ł tabliczki startej czekolady, jeśli chcemy mieć jej wi&oacute;rki w masie lodowej lub posypać deser</li></ul>";
        private const string lodyCzekoladoweOpis = "<p>W rondelku nad gotującą się wodą stopić czekoladę. Oddzielić dokładnie ż&oacute;łtka od białek, ż&oacute;łtka ubić z cukrem na jasny krem, dodać łyżkę wody i wanilię. Białka ubić osobno, na sztywną pianę.W trzecim pojemniku ubić na sztywno śmietankę. Do ubitych ż&oacute;łtek dodawać powoli, mieszając łyżką stopioną, lekko ostudzoną czekoladę, następnie ubitą śmietankę i na końcu po łyżce piany z białek. Przełożyć do plastikowego, zamykanego pojemnika i wstawić do zamrażarki na kilka godzin.</p>";

        private const string salatkaWarzywnaSkladniki = "<h2><strong>Składniki</strong></h2><ul><li>2 ziemniaki</li><li>1 marchew</li><li>1 cebula</li><li>1/2 puszki zielonego groszku</li><li>2 rzodkiewki</li><li>kawałek świeżego og&oacute;rka (mniej więcej 6-8 cm)</li><li>1 nieduży por (zielona część)</li><li>2 jajka</li><li>1,5 łyżki musztardy</li><li>2 łyżki majonezu</li><li>s&oacute;l i pieprz do smaku</li></ul>";
        private const string salatkaWarzywnaOpis = "<h3>Etapy przygotowania</h3><div id='intertext'><p><strong>Sałatka warzywna</strong> - c&oacute;ż może być prostszego? Wiem, znajdzie się kilka dań, ale naprawdę <strong>sałatka warzywna</strong> w moim wykonaniu to po prostu rach, ciach. A na dodatek moja <strong>sałatka warzywna</strong> zachwyci każdego :)</p><p>Kroimy i gotujemy warzywa:</p><p>Ziemniaki umyć i ugotować w mundurkach. Obrać i pokroić w kostkę. Marchew obrać i ugotować do miękkości, a potem r&oacute;wnież pokroić w kostkę. Cebulę obrać i drobno pokroić.Pora pokroić w cieniutkie krążki. Rzodkiewki i og&oacute;rka pokroić w kosteczkę.</p><p>Potem groszek odsączyć z zalewy.</p><p>Jajka ugotować na twardo, ostudzić, obrać i drobno pokroić.</p><p>Wszystkie warzywa oraz jajka dokładnie wymieszać.</p><p>Doprawić solą i pieprzem. Dodać musztardę oraz majonez i r&oacute;wnież wymieszać.</p><p><strong>Sałatka warzywna</strong> gotowa&nbsp; - starczy na kilka dni :)</p></div>";

        private const string salatkaOwocowaSkladniki = "<p><span style='text-decoration: underline;'>Składniki:</span></p><ul><li>kilka jasnych winogron</li><li>1 gruszka</li><li>2 poł&oacute;wki brzoskwiń z puszki</li><li>1 banan</li><li>250 g truskawek</li><li>1 łyżka miodu</li><li>1 łyżeczka soku z cytryny</li></ul><p><span style='text-decoration: underline;'>Do podania:</span></p><ul><li>serek waniliowy</li></ul>";
        private const string salatkaOwocowaOpis = "<div>Gruszkę i banana obieram. Umyte winogrona przekrawam na poł&oacute;wki. Truskawki myję, usuwam szypułki i kroję na 2- 4 części. Banana kroję w plasterki, a gruszkę i poł&oacute;wki brzoskwiń na mniejsze kawałki. Banana skrapiam sokiem z cytryny. W misce umieszczam wszystkie składniki, dosładzam miodem i ew. doprawiam sokiem z cytryny. Dokładnie mieszam. Wstawiam do lod&oacute;wki na 1 godzinę w celu schłodzenia. Podaję z serkiem waniliowym.</div><div>&nbsp;</div><div>Smacznego! :)</div>";

        private const string lodyWanilioweSkladniki = "<h3><strong>Składniki :</strong></h3><ul><li>300 ml śmietanki krem&oacute;wki 36%</li><li>2 jajka ( skorupki sparzone wrzątkiem )</li><li>100g cukru</li><li>1 łyżka wody przegotowanej</li><li>1 laska wanilii</li></ul>";
        private const string lodyWanilioweOpis = "<p><strong>Spos&oacute;b przygotowania :</strong></p><ol><li>Przygotowujemy 3 miski . W jednej umieszczamy śmietankę , w drugiej białka a w trzeciej ż&oacute;łtka .</li><li>Do ż&oacute;łtek dodajemy 50g cukru i 1 łyżkę wody . Miksujemy aż masa będzie biała i puszysta .</li><li>Białka ubijamy na sztywno , pod koniec ubijania dodajemy resztę cukru i ubijamy dalej .</li><li>Śmietankę także ubijamy na sztywno .</li><li>Do ż&oacute;łtek dodajemy stopniowo po łyżce białka i mieszamy delikatnie trzepaczką . Następnie dodajemy śmietankę , r&oacute;wnież stopniowo i delikatnie mieszamy . Na końcu dodajemy nasionka z wnętrza wanilii , mieszamy i przekładamy do pojemniczk&oacute;w . Mi wyszło ok. 1,5 litra lod&oacute;w . Zamrażamy min. 3 godz. . Z racji , że używamy surowych jajek należy lody zjeść w ciągu tygodnia .</li></ol>";

        private const string pastaJajecznaSkladniki = "<h3>Składniki</h3><p><small>ilość porcji:<span class='accent'>&nbsp;6&nbsp;</span></small></p><ul><li><span data-original='4 jajka'>4 jajka</span></li><li><span data-original='2 kostki serka topionego (najlepiej emmentalera, musi być miękki)'>2 kostki serka topionego (najlepiej emmentalera, musi być miękki)</span></li><li><span data-original='1 łyżka masła o temperaturze pokojowej'>1 łyżka masła o temperaturze pokojowej</span></li><li><span data-original='1 nieduża cebula'>1 nieduża cebula</span></li><li><span data-original='s&oacute;l i pieprz do smaku'>s&oacute;l i pieprz do smaku</span></li><li><span data-original='opcjonalnie: zielona pietruszka'>opcjonalnie: zielona pietruszka</span></li></ul>";
        private const string pastaJajecznaOpis = "<h2>Spos&oacute;b przygotowania</h2><p><small>Przygotowanie: <span class='accent'>10</span>min. &nbsp;&rsaquo;&nbsp; Gotowanie: <span class='accent'>5</span>min. &nbsp;&rsaquo;&nbsp; Gotowe w: <span class='accent'>15</span>min.&nbsp;</small></p><ol><li>Jajka ugotować na twardo, przestudzić, obrać. Kiedy będą całkiem zimne zetrzeć na tarce na najmniejszych oczkach.</li><li>W miseczce rozgnieść widelcem serek topiony z masłem. Jeśli masa będzie bardzo gęsta i ciężko się ją będzie wyrabiać można dodać majonezu, albo więcej miękkiego masła, albo poczekać aż się bardziej ogrzeje.</li><li>Do masy serkowej dodać jajka i pokrojoną drobniusieńko cebulę (opcjonalnie także zieloną pietruszkę). Doprawić do smaku solą i pieprzem (więcej pieprzu niż soli - serek już jest dość słony!).</li></ol><h4>Rada</h4><p>Zamiast surowej cebuli można dodać podduszonej na maśle. Po wyjęciu z lod&oacute;wki pasta jest gęsta i dość ciężko ją rozsmarować, więc lepiej wyjąć ją 15 minut przed planowanym użyciem :)</p>";

        private const string pastaLososiowaSkladniki = "<h2>Składniki</h2><ul><li>kawałek upieczonego lub ugotowanego łososia (bez ości) &ndash; u mnie zwykle jest to łosoś upieczony w folii aluminiowej</li><li>p&oacute;ł niedużej cebuli</li><li>ćwiartka czerwonej papryki</li><li>łyżka ketchupu</li><li>dwie łyżki oliwy z oliwek</li><li>sok wyciśnięty z cytryny</li><li>ząbek czosnku</li><li>s&oacute;l i pieprz do smaku</li></ul><p>&nbsp;</p>";
        private const string pastaLososiowaOpis = "<h2>Przygotowanie</h2><ol><li>Paprykę i cebulę kroję na kilka większych kawałk&oacute;w i umieszczam w pojemniku blendera, a następnie dość dokładnie rozdrabniam.</li><li>Upieczonego, chłodnego łososia dodaję do pojemnika z cebulą i papryką, a następnie dodaję wszystkie pozostałe składniki i miksuję aż do uzyskania jednolitej, gładkiej konsystencji.</li><li>Pastę nakładam na kromki bagietki z masłem i dekoruję natką pietruszki lub posiekanym szczypiorkiem.</li><li>Zawsze jest jej za mało ;)</li></ol>";

        private const string kulkiZiemniaczaneSkladniki = "<h3>&nbsp;<span style='text-decoration: underline;'><em>Składniki:</em></span></h3><ul><li><em>5 sztuk ziemniak&oacute;w</em></li><li><em>2 łyżki masła</em></li><li><em>3 łyżki cukru</em></li><li><em>1 szklanka oleju</em></li><li><em>&frac14; szklanki&nbsp; wywaru z warzyw</em></li><li><em>s&oacute;l</em></li><li><em>kilka gałązek natki pietruszki</em></li></ul>";
        private const string kulkiZiemniaczaneOpis = "<h3><span style='text-decoration: underline;'>Wykonanie:</span></h3><ol><li>Ziemniaki umyłam, obrałam i łyżeczką do melona wykroiłam kulki. Opłukałam w zimnej wodzie, wyłożyłam na papierowy ręcznik, żeby osuszyć. W rondlu rozgrzałam olej i na gorący&nbsp;włożyłam kulki ziemniak&oacute;w. Wyjęłam, kiedy się przyrumieniły.</li><li>Na patelnię wysypałam cukier i podgrzewałam na średnim gazie, żeby cukier nabrał złotego koloru. Nie mieszałam. Dodałam masło i jak się połączyło z karmelem wkładałam kulki o obracałam nimi, żeby całe pokryły się masa karmelową. Dodałam 3 łyżki wywaru. Podgrzewałam chwilę, &nbsp;do momentu, kiedy sos zaczął gęstnieć.</li><li>Kulki przełożyłam na p&oacute;łmisek...</li><li>... posypałam solą i natką pietruszki. Smaczne, delikatne, lekko słodkie... Po prostu inne niż zwykłe ziemniaczki, a do tego ładnie wyglądają &nbsp;i można skomponować z nimi każdą potrawę z mięsem i nie tylko, bo można je podać samodzielnie. To już zależy od Was.</li><li>U mnie pierwsza partia zniknęła zanim pomyślałam z czym je podam. Zr&oacute;bcie więc podw&oacute;jna porcję.</li></ol>";

        private const string paprykaFaszerowanaSkladniki = "<h3><span style='text-decoration: underline;'>Składniki:</span></h3><ul><li>6 dużych strąk&oacute;w papryki</li><li>35 dag wieprzowiny (np. łopatka wieprzowa)</li><li>15 dag wołowiny</li><li>20 dag ryżu</li><li>1 posiekana cebula</li><li>2 ząbki czosnku przeciśnięte przez praskę</li><li>s&oacute;l i pieprz</li><li>oliwa</li></ul><p>&nbsp;</p>";
        private const string paprykaFaszerowanaOpis = "<ol><li>Papryki&nbsp; umyłam, odcięłam g&oacute;rne części (kapelusiki) i wycięłam gniazda nasienne.&nbsp;</li><li>Mięso umyłam i zmieliłam. Cebulę zeszkliłam na oliwie.&nbsp; Natkę &nbsp;opłukałam i posiekałam. Do mięsa dodałam, ugotowany ryż, cebulę, &nbsp;czosnek i natkę, przyprawiłam do smaku solą i pieprzem.</li><li>Farsz &nbsp;wymieszałam i nałożyłam &nbsp;lekko ugniatając do wydrążonych strąk&oacute;w papryki.</li><li>Naczynie żaroodporne posmarowałam oliwą i ułożyłam w nim papryki, a następnie...</li><li>...wstawiłam do piekarnika w temperaturze 180 &deg;C na 45 minut. Po około 10 minutach podlałam wodą.Podałam, kiedy lekko ostygły.</li><li>Papryki nadziewane takim farszem zawsze smakują. Smacznego!</li></ol>";

        #endregion
    }
}