﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using cookwithme.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.ModelConfiguration;


namespace cookwithme.DAL
{
    public class KitchenContext : IdentityDbContext<ApplicationUser>
    {
        public KitchenContext()
            : base("KitchenContext")
        {

        }
        //public DbSet<ApplicationUser> users { get; set; }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<DishPicture> Pictures { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Vote> Votes { get; set; }
        public DbSet<ResetCode> ResetCodes { get; set; }
        public DbSet<Setting> Settings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

    }

}