﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using cookwithme.DAL;
using cookwithme.Models;
using System.Runtime.Caching;
using System.Web.UI;

namespace cookwithme.Views
{
    public class CategoriesController : Controller
    {
        private KitchenContext db = new KitchenContext();

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [OutputCache(Duration = 3600)]
        public ActionResult GetCategoriesThumbs()
        {
            var ExampleItems = new Dictionary<Category, Dish>();
            db.Categories.OrderBy(t => Guid.NewGuid()).ToList().ForEach(t => ExampleItems.Add(t, null));

            var dishes = db.Dishes.Where(t => t.accepted).OrderBy(t => Guid.NewGuid()).ToList();
            foreach (var item in dishes)
            {
                if (ExampleItems[item.category] == null)
                {
                    ExampleItems[item.category] = item;
                }
            }

            return View(ExampleItems);
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }

            int NumberOfDishes = 0;
            var DataFromCache = MemoryCache.Default["NumberOfDishesForCategoryPage"];
            if (DataFromCache != null)
            {
                Int32.TryParse(DataFromCache.ToString(), out NumberOfDishes);
            }
            if (NumberOfDishes == 0)
            {
                MemoryCache.Default["NumberOfDishesForCategoryPage"] = db.Settings.Where(t => t.Type == "NumberOfDishesForCategoryPage").First().Value;
                Int32.TryParse((MemoryCache.Default["NumberOfDishesForCategoryPage"]).ToString(), out NumberOfDishes);
            }

            ViewBag.TotalPages = Math.Ceiling((double)(db.Dishes.Where(t => t.accepted && t.CategoryID == id)).Count() / (double)(NumberOfDishes));


            return View(category);

        }

        [HttpGet]
        [OutputCache(Duration = 3600)]
        public PartialViewResult CategoryList(int? category)
        {
            ViewBag.category = category == null ? 0 : category;
            return PartialView("Menu", db.Categories.ToList());
        }









        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
