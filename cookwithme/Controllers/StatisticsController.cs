﻿using cookwithme.DAL;
using cookwithme.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace cookwithme.Controllers
{
    public class StatisticsController : Controller
    {
        private KitchenContext db = new KitchenContext();
        
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [OutputCache(Duration = 3600)]
        public PartialViewResult BestRated()
        {
            List<Dish> ListOfDishes = db.Dishes.ToList();
            Dictionary<Dish, double> AveragesOfDishes = new Dictionary<Dish,double>();
            ListOfDishes.ForEach(t => AveragesOfDishes.Add(t, t.votes.Count != 0 ? t.votes.Average(s => s.ocena) : 0));

            return PartialView(AveragesOfDishes.OrderByDescending(t => t.Value).Take(10));
        }

        [OutputCache(Duration = 3600)]
        public PartialViewResult MostCommented()
        {
            List<Dish> ListOfDishes = db.Dishes.ToList();
            Dictionary<Dish, int> CommentsOfDishes = new Dictionary<Dish, int>();
            ListOfDishes.ForEach(t => CommentsOfDishes.Add(t, t.comments.Count));

            return PartialView(CommentsOfDishes.OrderByDescending(t => t.Value).Take(10));
        }

        [OutputCache(Duration = 3600)]
        public PartialViewResult BestAddingDishes()
        {
            List<ApplicationUser> ListOfUsers = db.Users.ToList();
            Dictionary<ApplicationUser, int> AddedDishesOfUser = new Dictionary<ApplicationUser, int>();
            ListOfUsers.ForEach(t => AddedDishesOfUser.Add(t, t.dishes.Count));

            return PartialView(AddedDishesOfUser.OrderByDescending(t => t.Value).Take(10));
        }

        [OutputCache(Duration = 3600)]
        public PartialViewResult BestCommentator()
        {
            List<ApplicationUser> ListOfUsers = db.Users.ToList();
            Dictionary<ApplicationUser, int> AddedCommentsOfUser = new Dictionary<ApplicationUser, int>();
            ListOfUsers.ForEach(t => AddedCommentsOfUser.Add(t, t.comments.Count));

            return PartialView(AddedCommentsOfUser.OrderByDescending(t => t.Value).Take(10));
        }
    }
}