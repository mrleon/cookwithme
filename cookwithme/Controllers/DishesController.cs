﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using cookwithme.DAL;
using cookwithme.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.IO;
using System.Web.Helpers;
using System.Runtime.Caching;
using System.Web.UI;

namespace cookwithme.Controllers
{
    public class DishesController : Controller
    {
        private KitchenContext db = new KitchenContext();

        [HttpGet]
        public ActionResult Index()
        {

            int NumberOfDishes = 0;
            var DataFromCache = MemoryCache.Default["NumberOfDishesForPage"];
            if (DataFromCache != null)
            {
                Int32.TryParse(DataFromCache.ToString(), out NumberOfDishes);
            }
            if (NumberOfDishes == 0)
            {
                MemoryCache.Default["NumberOfDishesForPage"] = db.Settings.Where(t => t.Type == "NumberOfDishesForPage").First().Value;
                Int32.TryParse((MemoryCache.Default["NumberOfDishesForPage"]).ToString(), out NumberOfDishes);
            }

            ViewBag.TotalPages = Math.Ceiling((double)(db.Dishes.Where(t => t.accepted)).Count() / (double)(NumberOfDishes));

            return View();
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dish dish = db.Dishes.Find(id);
            if (dish == null || !dish.accepted)
            {
                return HttpNotFound();
            }
            string currentUserId = User.Identity.GetUserId();
            ViewBag.VoteOfCurrentUser = db.Votes.Where(t => t.ApplicationUserID == currentUserId && t.DishID == id).FirstOrDefault();
            return View(dish);
        }

        [HttpGet]
        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Server)]
        [AjaxOnly]
        public ActionResult GetDishesForPage(int? pageNumber)
        {
            int pageNumberForDB;
            if (!pageNumber.HasValue)
            {
                pageNumberForDB = 1;
            }
            else
            {
                pageNumberForDB = pageNumber.Value;
            }

            int ItemsForPage = 0;
            var DataFromCache = MemoryCache.Default["NumberOfDishesForPage"];
            if (DataFromCache != null)
            {
                Int32.TryParse(DataFromCache.ToString(), out ItemsForPage);
            }
            if (ItemsForPage == 0)
            {
                MemoryCache.Default["NumberOfDishesForPage"] = db.Settings.Where(t => t.Type == "NumberOfDishesForPage").First().Value;
                Int32.TryParse((MemoryCache.Default["NumberOfDishesForPage"]).ToString(), out ItemsForPage);
            }

            int SkippedItems = (pageNumberForDB - 1) * ItemsForPage;
            IQueryable<Dish> dishes = db.Dishes.Where(t => t.accepted).OrderBy(t => t.created).Skip(SkippedItems).Take(ItemsForPage);

            return PartialView(dishes.ToList());
        }

        [HttpGet]
        [AjaxOnly]
        [OutputCache(Duration = 3600, Location = OutputCacheLocation.Server)]
        public ActionResult GetCategoryDishesForPage(int categoryId, int? pageNumber)
        {
            int pageNumberForDB;
            if (!pageNumber.HasValue)
            {
                pageNumberForDB = 1;
            }
            else
            {
                pageNumberForDB = pageNumber.Value;
            }

            int ItemsForPage = 0;
            var DataFromCache = MemoryCache.Default["NumberOfDishesForCategoryPage"];
            if (DataFromCache != null)
            {
                Int32.TryParse(DataFromCache.ToString(), out ItemsForPage);
            }
            if (ItemsForPage == 0)
            {
                MemoryCache.Default["NumberOfDishesForCategoryPage"] = db.Settings.Where(t => t.Type == "NumberOfDishesForCategoryPage").First().Value;
                Int32.TryParse((MemoryCache.Default["NumberOfDishesForCategoryPage"]).ToString(), out ItemsForPage);
            }

            int SkippedItems = (pageNumberForDB - 1) * ItemsForPage;
            IQueryable<Dish> dishes = db.Dishes.Where(t => t.accepted && t.CategoryID == categoryId).OrderBy(t => t.created).Skip(SkippedItems).Take(ItemsForPage);

            return PartialView(dishes.ToList());
        }

        #region CREATE
        [HttpGet]
        [Authorize(Roles = "User")]
        public ActionResult Create()
        {
            var cats = db.Categories.ToList();
            ViewBag.Categories = cats;
            return View();
        }

        [Authorize(Roles = "User")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Title, Ingredients, Description, Category, Picture")] DishViewModelCreate dish)
        {
            string mime = "";
            byte[] picture = new byte[0];

            if (ModelState.IsValid)
            {
                HttpPostedFileBase file = Request.Files[0] == null ? null : Request.Files[0];
                if (file != null)
                {
                    mime = file.ContentType;
                    picture = new byte[file.ContentLength];
                    file.InputStream.Read(picture, 0, file.ContentLength);
                }
                var new_dish = new Dish
                {
                    ApplicationUserID = User.Identity.GetUserId(),
                    CategoryID = dish.Category,
                    created = DateTime.Now,
                    description = dish.Description,
                    ingredients = dish.Ingredients,
                    title = dish.Title,
                    accepted = false,
                };
                var new_picture = new DishPicture
                {
                    BigImage = picture,
                    BigMime = mime,
                };
                db.Dishes.Add(new_dish);
                db.SaveChanges();
                new_picture.ID = new_dish.ID;
                db.Pictures.Add(new_picture);
                db.SaveChanges();
                return RedirectToAction("Index", "Dishes");
            }
            else
            {
                var cats = db.Categories.ToList();
                ViewBag.Categories = cats;
                return View(dish);
            }
        }
        #endregion

        #region EDIT
        [HttpGet]
        [Authorize(Roles = "Moderator,Administrator")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dish dish = db.Dishes.Find(id);
            DishViewModelEdit model = new DishViewModelEdit
            {
                Title = dish.title,
                Category = dish.category.ID,
                Description = dish.description,
                Ingredients = dish.ingredients,
                HiddenID = dish.ID,
                Published = dish.accepted,
            };
            if (dish == null)
            {
                return HttpNotFound();
            }
            var cats = db.Categories.ToList();
            ViewBag.Categories = cats;
            ViewBag.SelectedCategory = dish.category.ID;
            return View(model);
        }
        [Authorize(Roles = "Moderator,Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Title, Ingredients, Description, Category, Difficulty, HiddenID, Published, Picture")] DishViewModelEdit model)
        {
            if (ModelState.IsValid)
            {
                Dish edited = new Dish
                {
                    ID = model.HiddenID,
                    title = model.Title,
                    ingredients = model.Ingredients,
                    description = model.Description,
                    CategoryID = model.Category,
                    accepted = model.Published,
                };

                string mime = "";
                byte[] picture = new byte[0];

                HttpPostedFileBase file = Request.Files[0] == null ? null : Request.Files[0];
                bool new_image = file.ContentLength != 0;

                DishPicture edited_picture = db.Pictures.First(t => t.ID == edited.ID);
                if (new_image)
                {
                    mime = file.ContentType;
                    picture = new byte[file.ContentLength];
                    file.InputStream.Read(picture, 0, file.ContentLength);

                    edited_picture.BigImage = picture;
                    edited_picture.BigMime = mime;
                }

                db.Dishes.Attach(edited);
                var entry = db.Entry(edited);
                entry.Property(t => t.title).IsModified = true;
                entry.Property(t => t.ingredients).IsModified = true;
                entry.Property(t => t.description).IsModified = true;
                entry.Property(t => t.CategoryID).IsModified = true;
                entry.Property(t => t.accepted).IsModified = true;
                db.SaveChanges();

                if (new_image)
                {
                    db.Pictures.Attach(edited_picture);
                    var entry2 = db.Entry(edited_picture);
                    entry2.Property(t => t.BigImage).IsModified = true;
                    entry2.Property(t => t.BigMime).IsModified = true;
                    db.SaveChanges();
                }
                return RedirectToAction("Manage", "Dishes");
            }
            return View(model);
        }
        #endregion

        #region MANAGE
        [HttpGet]
        [Authorize(Roles = "Moderator,Administrator")]
        public ActionResult Manage()
        {
            var dishes = db.Dishes.ToList();
            return View(dishes);
        }
        #endregion

        #region DELETE
        [HttpGet]
        [Authorize(Roles = "Moderator,Administrator")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dish dish = db.Dishes.Find(id);
            if (dish == null)
            {
                return HttpNotFound();
            }
            return View(dish);
        }

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Moderator,Administrator")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Dish dish = db.Dishes.Find(id);
            db.Dishes.Remove(dish);
            db.SaveChanges();
            return RedirectToAction("Manage", "Dishes");
        }
        #endregion

        #region COMMENT
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User")]
        public ActionResult AddComment()
        {
            string path = Request.Params["path"].ToString();
            int id = 0;
            if (!Int32.TryParse(Request.Params["id"].ToString(), out id))
            {
                return RedirectToLocal(path);
            }
            string tresc = Request.Params["comment_content"].ToString();

            Dish a = db.Dishes.FirstOrDefault(t => t.ID == id);
            if (a == null || !a.accepted || !User.Identity.IsAuthenticated)
            {
                return RedirectToLocal(path);
            }
            Comment nowy = new Comment { added = DateTime.Now, ApplicationUserID = User.Identity.GetUserId(), content = tresc, DishID = id };
            db.Comments.Add(nowy);
            db.SaveChanges();

            return RedirectToLocal(path);
        }



        #endregion

        #region VOTING

        [HttpPost]
        [Authorize]
        public ActionResult VoteForDish(int id, string vote)
        {
            string UserId = User.Identity.GetUserId();
            Dish dish = db.Dishes.FirstOrDefault(t => t.ID == id);
            if (dish == null || !dish.accepted)
            {
                return null;
            }
            int newVote = 0;
            if (!Int32.TryParse(vote, out newVote) || newVote < 1 || newVote > 5)
            {
                return null;
            }
            Vote VoteFromDb = db.Votes.FirstOrDefault(t => t.DishID == id && t.ApplicationUserID == UserId);
            if (VoteFromDb == null)
            {
                db.Votes.Add(new Vote { ApplicationUserID = UserId, DishID = id, ocena = newVote });
            }
            else if (VoteFromDb.ocena == newVote)
            {
                return null;
            }
            else
            {
                VoteFromDb.ocena = newVote;
                db.Votes.Attach(VoteFromDb);
                var entry = db.Entry(VoteFromDb);
                entry.Property(t => t.ocena).IsModified = true;
            }
            db.SaveChanges();

            return null;
        }


        #endregion

        #region DODATKI
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Dishes");
            }
        }

        #endregion


        #region IMAGES

        [OutputCache(Duration = 600)]
        public FileResult GetImage(int ID)
        {
            DishPicture tmp = db.Pictures.FirstOrDefault(t => t.ID == ID);
            if (tmp != null && tmp.BigImage != null && tmp.BigImage.Length != 0 && tmp.BigMime != "")
            {
                return File(tmp.BigImage, tmp.BigMime);
            }
            else
            {
                return new FilePathResult(HttpContext.Server.MapPath("~/DAL/ImageSeed/320x150.gif"), "image/gif");
            }
        }

        [OutputCache(Duration = 600)]
        public FileResult GetThumb(int ID)
        {
            DishPicture tmp = db.Pictures.FirstOrDefault(t => t.ID == ID);
            if (tmp != null && tmp.BigImage != null && tmp.BigImage.Length != 0 && tmp.BigMime != "")
            {
                return File(tmp.BigImage, tmp.BigMime);
            }
            else
            {
                return new FilePathResult(HttpContext.Server.MapPath("~/DAL/ImageSeed/320x150.gif"), "image/gif");
            }
        }
        #endregion

        #region SEARCH

        [HttpGet]
        public ActionResult Search()
        {
            return View();
        }

       

        [HttpGet]
        public ActionResult GetFoundDishes(string param, bool ifTitle)
        {
            List<Dish> model = new List<Dish>();

            string[] splittedParams = param.Split(' ');
            if (ifTitle)
            {
                model = db.Dishes.Where(t => t.accepted && splittedParams.Any(x => t.title.Contains(x))).ToList();
            }
            else
            {
                model = db.Dishes.Where(t => t.accepted && splittedParams.Any(x => t.description.Contains(x))).ToList();
            }

            IDictionary<Dish, int> counter = new Dictionary<Dish, int>();
            model.ForEach(t => counter.Add(t, 0));

            foreach (var item in model)
            {
                foreach (var key in splittedParams)
                {
                    if (ifTitle && item.title.Contains(key))
                    {
                        counter[item]++;
                    }
                    else if (item.description.Contains(key))
                    {
                        counter[item]++;
                    }
                }
            }
            counter.OrderBy(t => t.Value);
            return PartialView(counter);
        }

        #endregion









        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public class AjaxOnlyAttribute : ActionMethodSelectorAttribute
        {
            public override bool IsValidForRequest(ControllerContext controllerContext, System.Reflection.MethodInfo methodInfo)
            {
                return controllerContext.RequestContext.HttpContext.Request.IsAjaxRequest();
            }
        }
    }
}
