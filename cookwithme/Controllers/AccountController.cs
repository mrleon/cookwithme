﻿using cookwithme.DAL;
using cookwithme.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Web.Security;
using System.Text;
using System.Web.Hosting;

namespace cookwithme.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private UserManager<ApplicationUser> UserManager;
        private RoleManager<IdentityRole> RoleManager;
        private KitchenContext db = new KitchenContext();
        public AccountController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new KitchenContext())))
        {
            this.RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new KitchenContext()));
        }
        public AccountController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var UsersWithTheSameData = db.Users.Where(t => t.Email == model.Email || t.UserName == model.UserName);
            if (UsersWithTheSameData.Count() != 0)
            {
                ModelState.AddModelError(String.Empty, "Podana nazwa użytkownika lub adres email występują w serwisie");
                return View(model);
            }

            if (!IsPasswordStrongEnough(model.Password))
            {
                ModelState.AddModelError(String.Empty, "Podane hasło jest zby słabe. Użyj dużych i małych liter oraz co najmniej jednej cyfry.");
                return View(model);
            }

            var user = new ApplicationUser { Email = model.Email, UserName = model.UserName, registered = DateTime.Now, banned = false, };

            var result = await UserManager.CreateAsync(user, model.Password);
            UserManager.AddToRole(UserManager.FindByName(user.UserName).Id, "User");
            if (result.Succeeded)
            {
                //HostingEnvironment.QueueBackgroundWorkItem(t => SendWelcomeMessage(user.UserName, user.Email));

                await SignInAsync(user, isPersistent: false);
                return RedirectToAction("Index", "Dishes");
            }
            else
            {
                AddErrors(result);
                return View(model);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login(string returnURL)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToLocal(returnURL);
            }
            ViewBag.ReturnURL = returnURL;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnURL)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindAsync(model.UserName, model.Password);
                if (user != null)
                {
                    await SignInAsync(user, model.RememberMe);
                    return RedirectToLocal(returnURL);
                }
                else
                {
                    ModelState.AddModelError("", "Niepoprawna nazwa użytkownika lub hasło");
                }
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Dishes");
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public ActionResult Manage()
        {
            Dictionary<ApplicationUser, bool> slownik = new Dictionary<ApplicationUser, bool>();
            var lista = UserManager.Users.ToList().Where(t => UserManager.IsInRole(t.Id, "User") && !UserManager.IsInRole(t.Id, "Administrator")).ToList();
            lista.ForEach(t => slownik.Add(t, false));
            lista.Where(t => UserManager.IsInRole(t.Id, "Moderator")).ToList().ForEach(t => slownik[t] = true);

            return View(slownik);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult ModeratorSetter(string id)
        {
            ApplicationUser user = db.Users.Where(t => t.Id == id).FirstOrDefault();
            if (user != null)
            {
                if (UserManager.IsInRole(user.Id, "Moderator"))
                {
                    UserManager.RemoveFromRole(user.Id, "Moderator");
                }
                else
                {
                    UserManager.AddToRole(user.Id, "Moderator");
                }
            }
            return RedirectToAction("Manage", "Account");
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public ActionResult SetModerator(string name)
        {
            ApplicationUser user = db.Users.Where(t => t.UserName == name).FirstOrDefault();
            if (user != null)
            {
                UserManager.AddToRole(user.Id, "Moderator");
            }
            return RedirectToAction("Manage", "Account");
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public ActionResult RemoveModerator(string name)
        {
            ApplicationUser user = db.Users.Where(t => t.UserName == name).FirstOrDefault();
            if (user != null)
            {
                UserManager.RemoveFromRole(user.Id, "Moderator");
            }
            return RedirectToAction("Manage", "Account");
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgottenPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult ForgottenPassword(string mail)
        {
            ApplicationUser user = db.Users.FirstOrDefault(t => t.Email == mail);
            if (user == null)
            {
                ViewBag.Error = "Nie znaleziono takiego użytkownika";
                return View();
            }
            ResetCode code = new ResetCode { ApplicationUserID = user.Id, Code = Guid.NewGuid().ToString(), created = DateTime.Now };
            db.ResetCodes.Add(code);
            db.SaveChangesAsync();

            string ResetLink = Url.Action("ResetPassword", "Account", new { code = code.Code }, Request.Url.Scheme);

            //HostingEnvironment.QueueBackgroundWorkItem(t => SendResetLink(user.UserName, user.Email, ResetLink));

            return View("PasswordSent");
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            ResetCode codeFromDb = db.ResetCodes.Where(t => t.Code == code).FirstOrDefault();
            if (codeFromDb == null)
            {
                return View("CodeNotFound");
            }
            if (!IsSentWithin24Hours(codeFromDb.created))
            {
                return View("CodeNotFound");
            }
            ViewBag.code = code;
            ResetPasswordViewModel model = new ResetPasswordViewModel { code = code };
            return View(model);
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            ResetCode codeFromDb = db.ResetCodes.FirstOrDefault(t => t.Code == model.code);
            if (codeFromDb == null || !IsSentWithin24Hours(codeFromDb.created))
            {
                return View("CodeNotFound");
            }

            ApplicationUser user = codeFromDb.user;
            user.PasswordHash = new PasswordHasher().HashPassword(model.Password);

            db.Users.Attach(user);
            var entry = db.Entry(user);
            entry.Property(t => t.PasswordHash).IsModified = true;
            db.ResetCodes.RemoveRange(db.ResetCodes.Where(t => t.ApplicationUserID == codeFromDb.ApplicationUserID));
            db.SaveChanges();

            return View("PasswordChanged");

        }

        #region DODATKI
        private void SendResetLink(string userName, string mail, string resetLink)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Witaj, <b>" + userName + "</b>. ");
            sb.AppendLine("Poprosiłeś o zmianę hasła. Jeżeli nie wykonałeś takiej czynności, to zignoruj tego maila. Jeżeli chcesz zresetować hasło, to kliknij w poniższy link:");
            sb.AppendLine("<br /><a href='" + resetLink + "'>Klik!</a>");
            sb.AppendLine("<br />Pozdrawiamy! Ekipa CookWithMe");

            string subject = "CookWithMe - Resetuj hasło";

            GMailer.Send(subject, sb.ToString(), true, mail);
        }
        private void SendWelcomeMessage(string userName, string mail)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Witaj, <b>" + userName + "</b>. ");
            sb.AppendLine("Dziękujemy za rejestrację na witrynie CookWithMe! Życzymy samych delicji!");
            sb.AppendLine("Pozdrawiamy, Ekipa CookWithMe");

            string subject = "CookWithMe - Wiadomość powitalna";

            GMailer.Send(subject, sb.ToString(), true, mail);
        }
        private bool IsPasswordStrongEnough(string password)
        {
            if (password.Length < 6)
            {
                return false;
            }
            else if (!password.Any(char.IsDigit))
            {
                return false;
            }
            else if (!password.Any(char.IsUpper) || !password.Any(char.IsLower))
            {
                return false;
            }
            return true;
        }

        #endregion






        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private bool IsSentWithin24Hours(DateTime date)
        {
            if (date.Subtract(DateTime.Now).TotalHours <= 24)
                return true;

            return false;
        }



        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Dishes");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }

}