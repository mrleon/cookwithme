﻿using cookwithme.DAL;
using cookwithme.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace cookwithme.Controllers
{
    public class PartialsController : Controller
    {
        private KitchenContext db = new KitchenContext();

        [HttpGet]
        public PartialViewResult StarsForComment(Comment comm)
        {
            var vote = db.Votes.FirstOrDefault(item => item.DishID == comm.DishID && comm.ApplicationUserID == item.ApplicationUserID);
            if (vote != null)
            {
                ViewBag.Filled = vote.ocena;
                ViewBag.Blank = 5 - ViewBag.Filled;
            }
            else
            {
                ViewBag.Filled = 0;
                ViewBag.Blank = 5;
            }
            return PartialView("Stars");
        }

        [HttpGet]
        public PartialViewResult StarsForDish(double average)
        {
            ViewBag.Title = average;
            ViewBag.Filled = (int)average;
            ViewBag.Blank = 5 - ViewBag.Filled;

            return PartialView("Stars");
        }

        [HttpGet]
        [OutputCache(Duration = 3600)]
        public PartialViewResult Carousel()
        {
            List<string> imagesUrls = new List<string>();
            db.Dishes.Where(t => t.accepted).OrderBy(t => Guid.NewGuid()).Take(3).ToList().ForEach(t => imagesUrls.Add(Url.Action("GetImage", "Dishes", new { t.ID })));

            return PartialView(imagesUrls);
        }

    }
}