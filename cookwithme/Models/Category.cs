﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cookwithme.Models
{
    public class Category
    {
        public int ID { get; set; }
        public string name { get; set; }
        public virtual ICollection<Dish> dishes { get; set; }
        //
    }
}