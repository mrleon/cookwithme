﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace cookwithme.Models
{
    public class DishPicture
    {
        [Key, ForeignKey("Dish")]
        public int ID { get; set; }

        public byte[] BigImage { get; set; }
        public string BigMime { get; set; }

        public byte[] ThumbImage { get; set; }
        public string ThumbMime { get; set; }

        public virtual Dish Dish { get; set; }
    }
}