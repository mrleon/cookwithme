﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace cookwithme.Models
{
    public class Vote
    {
        [ForeignKey("")]
        public string ApplicationUserID { get; set; }
        public int ID { get; set; }
        public int DishID { get; set; }
        //public int CommentID { get; set; }
        //
        [Range(1,5)]
        public int ocena { get; set; }
    }
}