﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace cookwithme.Models
{
    public class Dish
    {
        public int ID { get; set; }
        public int CategoryID { get; set; }
        public string ApplicationUserID { get; set; }
        public string title { get; set; }
        public bool accepted { get; set; }
        public string ingredients { get; set; }
        public string description { get; set; }
        public DateTime created { get; set; }
        public Difficulty level { get; set; }
        //
        //
        public virtual DishPicture picture { get; set; }
        public virtual ApplicationUser user { get; set; }
        public virtual Category category { get; set; }
        public virtual ICollection<Comment> comments { get; set; }
        public virtual ICollection<Vote> votes { get; set; }
    }
    public enum Difficulty
    {
        [Display(Name="Bardzo łatwy")]
        Very_easy = 1,
        [Display(Name = "Łatwy")]
        Easy = 2,
        [Display(Name = "Średni")]
        Medium = 3,
        [Display(Name = "Trudny")]
        Hard = 4,
        [Display(Name = "Bardzo trudny")]
        Very_hard = 5,
    }
}