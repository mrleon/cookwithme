﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace cookwithme.Models
{
    public static class GMailer
    {
        private static string GmailUsername { get; set; }
        private static string GmailPassword { get; set; }
        private static string GmailHost { get; set; }
        private static int GmailPort { get; set; }
        private static bool GmailSSL { get; set; }

        static GMailer()
        {
            GmailHost = "smtp.gmail.com";
            GmailPort = 587; // Gmail can use ports 25, 465 & 587; but must be 25 for medium trust environment.
            GmailSSL = true;
            GmailUsername = "info.cookwithme@gmail.com";
            GmailPassword = "cookwithme";
        }

        public static void Send(string subject, string body, bool isHtml, string receiver)
        {
            SmtpClient smtp = new SmtpClient();
            smtp.Host = GmailHost;
            smtp.Port = GmailPort;
            smtp.EnableSsl = GmailSSL;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            
            smtp.Credentials = new NetworkCredential(GmailUsername, GmailPassword);

            using (var message = new MailMessage(GmailUsername, receiver))
            {
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = isHtml;
                try
                {
                    smtp.Send(message);
                }
                catch (Exception e) { }
            }
        }

    }
}