﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace cookwithme.Models
{
    public class DishViewModelCreate
    {
        [Required]
        [Display(Name = "Nazwa")]
        public string Title { get; set; }

        [Display(Name = "Obrazek")]
        public string Picture { get; set; }

        [Required]
        [Display(Name = "Składniki")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string Ingredients { get; set; }

        [Required]
        [Display(Name = "Opis")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Kategoria")]
        public int Category { get; set; }
    }
    public class DishViewModelEdit
    {
        [Required]
        [Display(Name = "Nazwa")]
        public string Title { get; set; }

        [Display(Name = "Obrazek")]
        public string Picture { get; set; }

        [Required]
        [Display(Name = "Składniki")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string Ingredients { get; set; }

        [Required]
        [Display(Name = "Opis")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Kategoria")]
        public int Category { get; set; }


        [Display(Name = "Opublikować?")]
        public bool Published { get; set; }

        public int HiddenID { get; set; }





    }
}