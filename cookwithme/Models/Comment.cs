﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace cookwithme.Models
{
    public class Comment
    {
        [Key]
        public int ID { get; set; }
        public int DishID { get; set; }
        public string ApplicationUserID { get; set; }
        public string content { get; set; }
        public DateTime added { get; set; }

        public virtual ApplicationUser user { get; set; }
    }
}