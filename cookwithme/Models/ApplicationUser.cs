﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace cookwithme.Models
{
    public class ApplicationUser : IdentityUser
    {
        public DateTime registered { get; set; }
        public bool? banned { get; set; }
        public virtual ICollection<Dish> dishes { get; set; }
        public virtual ICollection<Comment> comments { get; set; }
        public virtual ICollection<Vote> votes { get; set; }
    }
}
