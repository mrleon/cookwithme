﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cookwithme.Models
{
    public class Setting
    {
        public int ID { get; set; }

        public string Type { get; set; }

        public string Value { get; set; }
    }
}