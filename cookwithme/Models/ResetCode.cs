﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cookwithme.Models
{
    public class ResetCode
    {
        public int ID { get; set; }
        public string ApplicationUserID { get; set; }
        public string Code { get; set; }
        public DateTime created { get; set; }

        public virtual ApplicationUser user { get; set; }
    }
}